# README #


### What is this repository for? ###

* WebmodulesBundle is a bundle to add a different modules to a web (blog, sections...)

### How do I get set up? ###

Just install it via composer and you can add a service in your app to check the permissions before change modules in the database (add, edit, delete...)

##Add it to AppKernel.php.##
new \Greetik\WebmodulesBundle\WebmodulesBundle()


##In the config.yml you can add your own service##
webmodules:
    permsservice: app.webmodules
    interface: AppBundle:Webmodule

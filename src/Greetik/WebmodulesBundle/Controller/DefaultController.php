<?php

namespace Greetik\WebmodulesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\WebmodulesBundle\Entity\Webmodule;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {

    /**
     * Render all the webmodules of the user
     * 
     * @author Pacolmg
     */
    public function indexAction() {

        return $this->render($this->getParameter('webmodules.interface').':index.html.twig', array(
                    'data' => $this->get($this->getParameter('webmodules.permsservice'))->getAllWebmodulesByProject(),
                    'insertAllow' => $this->get($this->getParameter('webmodules.permsservice'))->getWebmodulePerm('insert')
        ));
    }

    /**
     * View an individual webmodule, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function viewAction($id) {
        $webmodule = $this->get($this->getParameter('webmodules.permsservice'))->getWebmodule($id);
        $editForm = $this->createForm(\Greetik\WebmodulesBundle\Form\Type\WebmoduleType::class, $this->get('webmodules.tools')->getWebmoduleObject($id));
        return $this->render($this->getParameter('webmodules.interface').':view.html.twig', array(
                    'item' => $webmodule,
                    'new_form' => $editForm->createView(),
                    'modifyAllow' => $this->get($this->getParameter('webmodules.permsservice'))->getWebmodulePerm('modify', $id)
        ));
    }

    /**
     * Get the data of a new Webmodule by Webmodule and persis it
     * 
     * @param Webmodule $item is received by Webmodule Request
     * @author Pacolmg
     */
    public function insertAction(Request $request) {
        $webmodule = new Webmodule();

        $newForm = $this->createForm(\Greetik\WebmodulesBundle\Form\Type\WebmoduleType::class, $webmodule);

        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                $this->get($this->getParameter('webmodules.permsservice'))->insertWebmodule($webmodule);
                return $this->redirect($this->generateUrl('webmodule_listwebmodules'));
            }
        }

        return $this->render($this->getParameter('webmodules.interface').':insert.html.twig', array('new_form' => $newForm->createView()));
    }

    /**
     * Edit the data of an webmodule
     * 
     * @param int $id is received by Get Request
     * @param Webmodule $item is received by Webmodule Request
     * @author Pacolmg
     */
    public function modifyAction(Request $request, $id) {
        $webmodule = $this->get('webmodules.tools')->getWebmoduleObject($id);

        $editForm = $this->createForm(\Greetik\WebmodulesBundle\Form\Type\WebmoduleType::class, $webmodule);

        $oldtype = $webmodule->getModuletype();

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try{
                    $this->get($this->getParameter('webmodules.permsservice'))->modifyWebmodule($webmodule, $oldtype);
                }catch(\Exception $e){
                    $this->addFlash('error', $e->getMessage());
                }
                return $this->redirect($this->generateUrl('webmodule_viewwebmodule', array('id' => $id)));
            }
        }
        return $this->viewAction($id, $editForm);
    }

    public function deleteAction($id) {

        $webmodule = $this->get('webmodules.tools')->getWebmoduleObject($id);
        $this->get($this->getParameter('webmodules.permsservice'))->deleteWebmodule($webmodule, false);

        return $this->redirect($this->generateUrl('webmodule_listwebmodules'));
    }

}

<?php

namespace Greetik\WebmodulesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\WebmodulesBundle\Entity\Sectionmodule;
use Greetik\WebmodulesBundle\Form\Type\SectionmoduleType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Manage the sectionmodules defined for section
 *
 * @author Pacolmg
 */
class SectionmoduleController extends Controller {

    /**
     * Show the new module insert form
     * 
     * @author Pacolmg
     */
    public function insertformAction($id_section, $id = '') {

        if (!empty($id)) {
            $sectionmodule = $this->get('webmodules.sectionmodules')->getSectionmoduleObject($id);
        } else {
            $sectionmodule = new Sectionmodule();
        }

        $newForm = $this->createForm(SectionmoduleType::class, $sectionmodule, array('_modules' => $this->get($this->getParameter('webmodules.permsservice'))->getAllWebmodulesByProject()));

        return $this->render('WebmodulesBundle:Sectionmodule:insert.html.twig', array('new_form' => $newForm->createView(), 'id_section' => $id_section, 'id' => $id));
    }

    protected function getSectionsandModules($id_project, $id_section = '') {
        if ($id_section) {
            $sections = array($this->get($this->getParameter('treesections.permsservice'))->getTreesection($id_section));
        } else
            $sections = $this->get($this->getParameter('treesections.permsservice'))->getAllTreesectionsByProject($id_project);

        $modules = $this->get('webmodules.modules')->getModules();

        return array($sections, $modules);
    }

    /**
     * Edit the data of an sectionmodule or insert a new one
     * 
     * @param int $id is received by Get Request
     * @param Sectionmodule $item is received by Post Request
     * @author Pacolmg
     */
    public function insertmodifyAction(Request $request, $id_section, $id = '') {
        $item = $request->get('sectionmodule');

        if (!empty($id)) {
            $sectionmodule = $this->get('webmodules.sectionmodules')->getSectionmoduleObject($id);
            $editing = true;
            $id_section = $sectionmodule->getSection();
        } else {
            $sectionmodule = new Sectionmodule();
            $editing = false;
        }


        $editForm = $this->createForm(SectionmoduleType::class, $sectionmodule, array('_modules' => $this->get($this->getParameter('webmodules.permsservice'))->getAllWebmodulesByProject()));
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            try{
                $treesectionobject = $this->get('treesection.tools')->getTreesectionObject($id_section);
                if ($treesectionobject->getProject() == $sectionmodule->getModule())
                    throw new \Exception('No puede agregar este módulo a esta sección');
                if (!$this->get($this->getParameter('treesections.permsservice'))->getTreesectionPerm('modify', $treesectionobject))
                    throw new \Exception('No tienes permiso para modificar la sección');
            }catch(\Exception $e){
                return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
            }

            if ($editing) {
                try {
                    $this->get('webmodules.sectionmodules')->modifySectionmodule($sectionmodule);
                } catch (\Exception $e) {
                    return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
                }
            } else {
                try {
                    $sectionmodule->setSection($id_section);
                    $sectionmodule->setNumorder(0);
                    $this->get('webmodules.sectionmodules')->insertSectionmodule($sectionmodule);
                } catch (\Exception $e) {
                    return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
                }
            }

            return $this->render('WebmodulesBundle:Sectionmodule:index.html.twig', array('sectionmodules' => $this->get('webmodules.sectionmodules')->getModules($sectionmodule->getSection()), 'itemid' => $id_section, 'modifyAllow' => true));
        } else {
            $errors = (string) $editForm->getErrors(true, true);
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $errors)), 200, array('Content-Type' => 'application/json'));
        }
        return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error Desconocido')), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * Delete a sectionmodule
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function dropAction($id) {
        $sectionmodule = $this->get('webmodules.sectionmodules')->getSectionmoduleObject($id);

        try {
            if (!$this->get($this->getParameter('treesections.permsservice'))->getTreesectionPerm('modify', $this->get('treesection.tools')->getTreesectionObject($sectionmodule->getSection())))
                throw new \Exception('No tienes permiso para modificar la sección');
            $this->get('webmodules.sectionmodules')->deleteSectionmodule($sectionmodule);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }

        return $this->render('WebmodulesBundle:Sectionmodule:index.html.twig', array('itemid' => $sectionmodule->getId(), 'modifyAllow' => true, 'sectionmodules' => $this->get('webmodules.sectionmodules')->getModules($sectionmodule->getSection())));
    }

    public function putupAction(Request $request) {
        //return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$request->get('id'))), 200, array('Content-Type'=>'application/json'));
        $sectionmodule = $this->get('webmodules.sectionmodules')->getSectionmoduleObject($request->get('id'));
        try {
            if (!$this->get($this->getParameter('treesections.permsservice'))->getTreesectionPerm('modify', $this->get('treesection.tools')->getTreesectionObject($sectionmodule->getSection())))
                throw new \Exception('No tienes permiso para modificar la sección');
            $this->get('webmodules.sectionmodules')->putupSectionmodule($sectionmodule);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }
        return $this->render('WebmodulesBundle:Sectionmodule:index.html.twig', array('itemid' => $sectionmodule->getId(), 'modifyAllow' => true, 'sectionmodules' => $this->get('webmodules.sectionmodules')->getModules($sectionmodule->getSection())));
    }

    public function putdownAction(Request $request) {
        //return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$request->get('id'))), 200, array('Content-Type'=>'application/json'));
        $sectionmodule = $this->get('webmodules.sectionmodules')->getSectionmoduleObject($request->get('id'));
        try {
            if (!$this->get($this->getParameter('treesections.permsservice'))->getTreesectionPerm('modify', $this->get('treesection.tools')->getTreesectionObject($sectionmodule->getSection())))
                throw new \Exception('No tienes permiso para modificar la sección');
            $this->get('webmodules.sectionmodules')->putdownSectionmodule($sectionmodule);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }
        return $this->render('WebmodulesBundle:Sectionmodule:index.html.twig', array('itemid' => $sectionmodule->getId(), 'modifyAllow' => true, 'sectionmodules' => $this->get('webmodules.sectionmodules')->getModules($sectionmodule->getSection())));
    }

}

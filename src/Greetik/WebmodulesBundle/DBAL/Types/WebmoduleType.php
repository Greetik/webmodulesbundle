<?php
namespace Greetik\WebmodulesBundle\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class WebmoduleType extends AbstractEnumType
{
    const BLOG = 1;
    const EVENT = 2;
    const TREESECTION = 3;
    const WEBFORMS = 4;
    const CATALOG = 5;
    const MAP = 6;
    
    protected static $choices = [
        self::BLOG => 'Blog',
        self::EVENT => 'Eventos',
        self::TREESECTION => 'Secciones',
        self::WEBFORMS => 'Formularios',
        self::CATALOG => 'Catálogo',
        self::MAP => 'Mapas'
    ];
    
    protected static $singularnames = [
        self::BLOG => 'Post',
        self::EVENT => 'Evento',
        self::TREESECTION => 'Sección',
        self::WEBFORMS => 'Formulario',
        self::CATALOG => 'Catálogo',
        self::MAP => 'Mapa'
    ];
    
    protected static $colors = [
        self::BLOG => '#306b03',
        self::EVENT => '#0028cc',
        self::TREESECTION => '#a50b8e',
        self::WEBFORMS => '#e8f442',
        self::CATALOG => '#f23e2e',
        self::MAP => '#ffa32b'
    ];
    
    protected static $icons = [
        self::BLOG => 'file-text',
        self::EVENT => 'calendar',
        self::TREESECTION => 'sitemap',
        self::WEBFORMS => 'tasks',
        self::CATALOG => 'book',
        self::MAP => 'map'
    ];

    protected static $links = [
        self::BLOG => 'blog_listposts',
        self::EVENT => 'events_listevents',
        self::TREESECTION => 'treesection_listtreesections',
        self::WEBFORMS => 'webforms_listformfields',
        self::CATALOG => 'catalog_viewcatalog',
        self::MAP => 'gmap_viewmap'
    ];
    

    protected static $prefix = [
        self::BLOG => 'blog_',
        self::EVENT => 'events_',
        self::TREESECTION => 'treesection_',
        self::WEBFORMS => 'webforms_',
        self::CATALOG => 'catalog_',
        self::MAP => 'gmap_'
    ];    

    //obtiene el icono asociado a un tipo
    public function getIcon($type){
        return self::$icons[$type];
    }

    //obtiene el color asociado a un tipo
    public function getColor($type){
        return self::$colors[$type];
    }

    //obtiene los nombres en singular
    public function getSingularName($type){
        return self::$singularnames[$type];
    }

    //obtiene los nombres en singular
    public function getLink($type){
        return self::$links[$type];
    }

    //obtiene los prefijos
    public function getPrefix($type){
        return self::$prefix[$type];
    }
}
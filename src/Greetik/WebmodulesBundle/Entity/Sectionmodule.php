<?php

namespace Greetik\WebmodulesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sectionmodule
 *
 * @ORM\Table(name="sectionmodule", indexes={
 *      @ORM\Index(name="section", columns={"section"}), @ORM\Index(name="module", columns={"module"}),  @ORM\Index(name="numorder", columns={"numorder"})
 * })
 * @ORM\Entity(repositoryClass="Greetik\WebmodulesBundle\Repository\SectionmoduleRepository")
 */
class Sectionmodule
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="section", type="integer")
     */
    private $section;

    /**
    * @var integer
    * 
    * @ORM\Column(name="module", type="integer")
    */
    private $module;   
    
    /**
     * @var integer
     *
     * @ORM\Column(name="numorder", type="integer")
     */
    private $numorder;

    

    /**
     * Get module
     *
     * @return \Greetik\GwadminBundle\Entity\Module
     */
    public function getModule()
    {
        return $this->module;
    }
    
    public function getItemid(){
        return $this->section;
    }
    public function getItemtype(){
        return "";
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set section
     *
     * @param integer $section
     *
     * @return Sectionmodule
     */
    public function setSection($section)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return integer
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set module
     *
     * @param integer $module
     *
     * @return Sectionmodule
     */
    public function setModule($module)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Set numorder
     *
     * @param integer $numorder
     *
     * @return Sectionmodule
     */
    public function setNumorder($numorder)
    {
        $this->numorder = $numorder;

        return $this;
    }

    /**
     * Get numorder
     *
     * @return integer
     */
    public function getNumorder()
    {
        return $this->numorder;
    }
}

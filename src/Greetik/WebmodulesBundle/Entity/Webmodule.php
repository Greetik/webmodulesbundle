<?php

namespace Greetik\WebmodulesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Webmodule
 *
 * @ORM\Table(name="webmodule")
 * @ORM\Entity(repositoryClass="Greetik\WebmodulesBundle\Repository\WebmoduleRepository")
 */
class Webmodule
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="project", type="integer", nullable=true)
     */
    private $project;

    /**
     * @var WebmoduleType
     *
     * @ORM\Column(name="moduletype", type="WebmoduleType")
     */
    private $moduletype;

    /**
     * @var boolean
     *
     * @ORM\Column(name="hidden", type="integer", nullable=true)
     */
    private $hidden=false;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Webmodule
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set moduletype
     *
     * @param WebmoduleType $moduletype
     *
     * @return Webmodule
     */
    public function setModuletype($moduletype)
    {
        $this->moduletype = $moduletype;

        return $this;
    }

    /**
     * Get moduletype
     *
     * @return WebmoduleType
     */
    public function getModuletype()
    {
        return $this->moduletype;
    }

    /**
     * Set project
     *
     * @param integer $project
     *
     * @return Webmodule
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return integer
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set hidden
     *
     * @param integer $hidden
     *
     * @return Webmodule
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get hidden
     *
     * @return integer
     */
    public function getHidden()
    {
        return $this->hidden;
    }
}

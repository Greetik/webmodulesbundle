<?php
    namespace Greetik\WebmodulesBundle\Form\Type;
    
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Doctrine\ORM\EntityRepository;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModulesType
 *
 * @author Paco
 */
class WebmoduleType extends AbstractType{
    
    public function __construct() {
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options){
        
            $builder
            ->add('moduletype')
            ->add('name');
                            
    }
    
    public function getName(){
        return 'Webmodule';
    }
    
    public function getDefaultOptions(array $options){
        return array( 'data_class' => 'Greetik\WebmodulesBundle\Entity\Webmodule');
    }
}


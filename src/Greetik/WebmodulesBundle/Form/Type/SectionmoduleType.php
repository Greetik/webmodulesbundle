<?php
    namespace Greetik\WebmodulesBundle\Form\Type;
    
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Doctrine\ORM\EntityRepository;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    use Symfony\Component\OptionsResolver\OptionsResolver;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SectionmoduleType
 *
 * @author Paco
 */
class SectionmoduleType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options){
        
        $modules = array();
        foreach($options['_modules'] as $k=>$v){
            $modules[$v['name']] = $v['id'];
        }
            $builder
            ->add('module', ChoiceType::class, array('required'=>true, 'choices'=>$modules));                            
    }
    
    public function getName(){
        return 'Sectionmodule';
    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array( 'data_class' => 'Greetik\WebmodulesBundle\Entity\Sectionmodule', '_modules'=>array()));
    }
    
}


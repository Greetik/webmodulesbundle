<?php

namespace Greetik\WebmodulesBundle\Twig;

use Greetik\WebmodulesBundle\DBAL\Types\WebmoduleType;

class AppExtension extends \Twig_Extension {

    private $webmodules;

    public function __construct($_webmodules) {
        $this->webmodules = $_webmodules;
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('webmoduleicon', array($this, 'webmoduleicon')), //Icono de un módulo
            new \Twig_SimpleFilter('webmodulecolor', array($this, 'webmodulecolor')), //Color de un módulo
            new \Twig_SimpleFilter('webmodulename', array($this, 'webmodulename')), //Nombre de un módulo
            new \Twig_SimpleFilter('webmodulelink', array($this, 'webmodulelink')), //Link de un módulo
            new \Twig_SimpleFilter('webmoduleprefix', array($this, 'webmoduleprefix')), //Prefijo de un módulo para marcar un menú activo
            new \Twig_SimpleFilter('webmoduletitle', array($this, 'webmoduletitle'))//get the title of the module
        );
    }


    /*Devuelve el icono de un módulo */
    public function webmoduleicon($type){
        return WebmoduleType::getIcon($type);
    }
    
    /*Devuelve el color de un módulo */
    public function webmodulecolor($type){
        return WebmoduleType::getColor($type);
    }
    
    /*Devuelve el nombre de un módulo */
    public function webmodulename($type){
        return WebmoduleType::getReadableValue($type);
    }
    
    /*Devuelve el enlace de un módulo */
    public function webmodulelink($type){
        return WebmoduleType::getLink($type);
    }
    
    /*Devuelve el nombre de un módulo */
    public function webmoduletitle($id){
        return $this->webmodules->getWebmoduleName($id);
    }
    
    /*Devuelve el prefijo para marcar el menú activo */
    public function webmoduleprefix($type){
        return WebmoduleType::getPrefix($type);
    }
    
    
    
    public function getName() {
        return 'webmodules_extension';
    }

}

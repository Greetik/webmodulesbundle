<?php

namespace Greetik\WebmodulesBundle\Services;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sectionmodule Tools
 *
 * @author Pacolmg
 */
class Sectionmodules {

    private $em;
    private $modules;
    private $beinterface;

    public function __construct($_entityManager, $_modules,$_beinterface) {
        $this->em = $_entityManager;
        $this->modules = $_modules;
        $this->beinterface= $_beinterface;
    }

    public function getModules($idsection){
        foreach($data = $this->em->getRepository('WebmodulesBundle:Sectionmodule')->getModules($idsection) as $k=>$v){
            $data[$k] = $this->setModuleData($v);
        }
        return $data;
    }
    protected function setModuleData($module){
        $module['moduledata'] = $this->modules->getWebmodule($module['module']);
        return $module;
    }
    
    public function getSectionmodule($idsection){
        return $this->em->getRepository('WebmodulesBundle:Sectionmodule')->getSectionmodule($idsection);
    }

    public function getSectionmoduleObject($id) {
        $sectionmodule = $this->em->getRepository('WebmodulesBundle:Sectionmodule')->findOneById($id);
        return $sectionmodule;
    }

    public function modifySectionmodule($sectionmodule) {
        if ($this->checkifexist($sectionmodule->getModule(), $sectionmodule->getSection(), $sectionmodule->getId()))
            throw new \Exception('Ya está asociado el módulo a esa sección');
        $this->em->persist($sectionmodule);
        $this->em->flush();
    }

    public function insertSectionmodule($sectionmodule) {
        if ($this->checkifexist($sectionmodule->getModule(), $sectionmodule->getSection()))
            throw new \Exception('Ya está asociado el módulo a esa sección');
        $sectionmodule->setNumorder($this->em->getRepository('WebmodulesBundle:Sectionmodule')->getNumelemsBySection($sectionmodule->getSection())+1);
        $this->em->persist($sectionmodule);
        $this->em->flush();
    }

    protected function checkifexist($module, $section, $id = '') {
        $auxsectionmodule = $this->em->getRepository('WebmodulesBundle:Sectionmodule')->findBy(array('module' => $module, 'section' => $section));
        if (count($auxsectionmodule) > 0) {
            if (empty($id)) return true;
            if (!empty($id) && $auxsectionmodule[0]->getId() != $id)
                return true;
        }
        return false;
    }

    public function deleteSectionmodule($sectionmodule) {
        $section = $sectionmodule->getSection();
        $this->em->remove($sectionmodule);
        $this->em->flush();
        
        $this->beinterface->reorderItemElems($this->em->getRepository('WebmodulesBundle:Sectionmodule')->findBy(array ('section'=>$section), array('numorder'=>'ASC')));
    }

    public function getSectionmodulesByModule($module) {
        return $this->em->getRepository('WebmodulesBundle:Sectionmodule')->findByModule($module);
    }



    
    
    
    
    
    
    
    

    public function getSectionmodulesBySection($section) {
        if (!$this->getSectionmodulePerm($section, 'view'))
            throw new \Exception('No tienes permiso para ver la sección');
        return $this->em->getRepository('WebmodulesBundle:Sectionmodule')->findBy(array ('section'=>$section), array('numorder'=>'ASC'));
    }



    public function getSectionmodulePerm($section, $type = '') {
        if ($type=='view') return true;
        return $this->__context->isGranted('ROLE_ADMIN');
    }

    protected function checkIfSectionEqualsToModule($sectionmodule) {
        $section = $this->sections->getTreesection($sectionmodule->getSection())->getProject();
        return ($section == $sectionmodule->getModule()->getId());
    }

    public function putupSectionmodule($sectionmodule){
        $this->beinterface->moveItemElem('WebmodulesBundle:Sectionmodule', $sectionmodule->getId(), $sectionmodule->getNumorder()-2, $sectionmodule->getNumorder(), $this->em->getRepository('WebmodulesBundle:Sectionmodule')->getNumelemsBySection($sectionmodule->getSection()), '', $sectionmodule->getSection() );
    }
    
    public function putdownSectionmodule($sectionmodule){
        if ($sectionmodule->getNumorder() < $this->em->getRepository('WebmodulesBundle:Sectionmodule')->getNumelemsBySection($sectionmodule->getSection()))
        $this->beinterface->moveItemElem('WebmodulesBundle:Sectionmodule', $sectionmodule->getId(), $sectionmodule->getNumorder(), $sectionmodule->getNumorder(), $this->em->getRepository('WebmodulesBundle:Sectionmodule')->getNumelemsBySection($sectionmodule->getSection()), '', $sectionmodule->getSection() );
    }
    
    
}

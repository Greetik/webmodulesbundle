<?php

namespace Greetik\WebmodulesBundle\Services;

use Greetik\WebmodulesBundle\Entity\Webmodule;
use Greetik\WebmodulesBundle\DBAL\Types\WebmoduleType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */
class Webmodulestools {

    private $em;
    private $catalog;

    public function __construct($_entityManager, $_catalog) {
        $this->catalog = $_catalog;
        $this->em = $_entityManager;
    }

    public function getWebmodulePerm($perm, $webmodule = '', $project = '') {
        return true;
    }

    public function getAllWebmodulesByProject($project = '') {
        if (empty($project))
            $data = $this->em->getRepository('WebmodulesBundle:Webmodule')->findAll();
        else
            $data = $this->em->getRepository('WebmodulesBundle:Webmodule')->findAllByProject($project);
        foreach ($data as $k => $v) {
            if ($v['hidden']) {
                unset($data[$k]);
                continue;
            }
            $data[$k] = $this->setData($v);
        }
        return $data;
    }

    public function getWebmodulesByType($type) {
        $data = $this->em->getRepository('WebmodulesBundle:Webmodule')->findByType($type);
        foreach ($data as $k => $v) {
            if ($v['hidden']) {
                unset($data[$k]);
                continue;
            }
            $data[$k] = $this->setData($v);
        }
        return $data;
    }

    public function getWebmodule($id) {
        return $this->setData($this->em->getRepository('WebmodulesBundle:Webmodule')->getWebmodule($id));
    }

    public function getWebmoduleName($id) {
        return $this->em->getRepository('WebmodulesBundle:Webmodule')->getWebmoduleName($id);
    }

    public function getWebmoduleObject($id) {
        $webmodule = $this->em->getRepository('WebmodulesBundle:Webmodule')->findOneById($id);

        return $webmodule;
    }

    public function modifyWebmodule($webmodule, $oldtype='') {
        if (!empty($oldtype) && $oldtype!= $webmodule->getModuletype() && ($oldtype== WebmoduleType::CATALOG  || $webmodule->getModuletype()== WebmoduleType::CATALOG ) ) throw new \Exception('Los catálogos no pueden cambiar de tipo, ni cambiar otro módulo a catálogo');        
        $this->em->flush();
    }

    public function insertWebmodule($webmodule, $project = null) {
        $webmodule->setProject($project);

        $this->em->persist($webmodule);
        $this->em->flush();
        $idwebmodule = $webmodule->getId();

        if ($webmodule->getModuletype() == WebmoduleType::CATALOG) {
            $form = new Webmodule();
            $form->setHidden(true);
            $form->setModuletype(WebmoduleType::WEBFORMS);
            $form->setName('Formulario de ' . $webmodule->getName());
            $idform = $this->insertWebmodule($form, $project);
            $categories = new Webmodule();
            $categories->setHidden(true);
            $categories->setModuletype(WebmoduleType::TREESECTION);
            $categories->setName('Categorías de ' . $webmodule->getName());
            $idcategories = $this->insertWebmodule($categories, $project);

            $config = new \Greetik\CatalogBundle\Entity\Catalogconfig;
            $config->setProject($idwebmodule);
            $config->setTreesection($idcategories);
            $config->setWebform($idform);
            $this->catalog->saveConfig($config);
        }

        return $idwebmodule;
    }

    public function deleteWebmodule($webmodule) {
        if (is_numeric($webmodule))
            $webmodule = $this->getWebmoduleObject($webmodule);

        if ($webmodule->getModuletype() == WebmoduleType::CATALOG) {
            $idform = $this->catalog->getCatalogForm($webmodule->getId(), true);
            $idcategories = $this->catalog->getCategories($webmodule->getId(), '', true);

            $this->deleteWebmodule($this->getWebmoduleObject($idform));
            $this->deleteWebmodule($this->getWebmoduleObject($idcategories));
        }

        $this->em->remove($webmodule);
        $this->em->flush();
    }

    protected function setData($webmodule) {
        if (class_exists(\Greetik\WebmodulesBundle\DBAL\Types\WebmoduleType::class)) {
            $webmodule['moduletypename'] = \Greetik\WebmodulesBundle\DBAL\Types\WebmoduleType::getReadableValue($webmodule['moduletype']);
        } else {
            $webmodule['moduletypename'] = \AppBundle\DBAL\Types\WebmoduleType::getReadableValue($webmodule['moduletype']);
        }
        return $webmodule;
    }

}
